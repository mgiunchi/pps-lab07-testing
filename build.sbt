val scalatest = "org.scalatest" % "scalatest_2.12" % "3.0.1" % "test"
lazy val root = (project in file(".")).settings (
  name:= "pps-lab07-testing", version := "1.0",
  organization := "unibo.pps", scalaVersion := "2.12.2",
    libraryDependencies ++= Seq(scalatest))