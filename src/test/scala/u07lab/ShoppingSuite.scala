package u07lab

import org.scalatest.FunSuite
import u07lab.TryShopping.{p1, p2}

class ShoppingSuite extends FunSuite{

  val cart = new BasicCart()
  val item1detail = new ItemDetails(2,new Price(30))
  val item1 = new Item(p1,item1detail)
  val catalog = new BasicCatalog(Map[Product,Price](
    p1 -> Price(78),
    p2 -> Price(34)
  ))

  test ("An empty Cart should have size 0") {
    assert(cart.size == 0)
  }

  test ("A Cart with0 2 items have size 2") {
    cart.add(item1)
    assert(cart.size == 1)
  }

  test ("Verify if total cost of item1 is price*quantity"){
    assert(cart.totalCost==30)
  }
}
